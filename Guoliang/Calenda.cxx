//
// Created by eguolyi on 2018-05-05.
//

#include "Calenda.hxx"
#include <ctime>
#include <iomanip>

Calenda::Calenda() {

    time_t now = time(0);
    // convert now to string form
    char *dt = ctime(&now);

    // convert now to tm struct for UTC

    tm *gmtm = gmtime(&now);
    year = gmtm->tm_year + 1900;
    month = gmtm->tm_mon + 1;
    weekDay = getWeekDay(year, month, 1);
    switch (month) {
        case 2:
            sizeOfMonth = 28;
            break;
        case 4:
        case 6:
        case 9:
        case 11:
            sizeOfMonth = 30;
            break;
        default:
            sizeOfMonth = 31;
    }
    for (auto i = 1U; i <= sizeOfMonth; i++)
        monthDetail.push_back(dateInfo{i});

}

void Calenda::fillMonthDetail() {


}

Calenda::Calenda(unsigned year, unsigned month) {

}


void Calenda::printByYear() const {

}

unsigned Calenda::setDays() {
    return 0;
}

void Calenda::printByMonth(std::ostream &out, std::string language) const {

    unsigned const WIDTH = 4;
    out.setf(std::ios::left);
    std::string strMonth;
    switch (month) {
        case 1:
            strMonth = "January";
            break;
        case 2:
            strMonth = "Feburary";
            break;
        case 3:
            strMonth = "March";
            break;
        case 4:
            strMonth = "April";
            break;
        case 5:
            strMonth = "May";
            break;
        case 6:
            strMonth = "June";
            break;
        case 7:
            strMonth = "July";
            break;
        case 8:
            strMonth = "August";
            break;
        case 9:
            strMonth = "September";
            break;
        case 10:
            strMonth = "October";
            break;
        case 11:
            strMonth = "November";
            break;
        case 12:
            strMonth = "December" ;
            break ;
    }

    out << strMonth << " " << year << std::endl;
    out << std::setw(WIDTH) << "Sun" << std::setw(WIDTH) << "Mon" << std::setw(WIDTH) << "Tue" << std::setw(WIDTH)
        << "Wed" << std::setw(WIDTH) << "Thu" << std::setw(WIDTH)
        << "Fri" << std::setw(WIDTH) << "Sat" << std::endl;

    out.setf(std::ios::left);
    // print leading space
    for (auto i = 0U; i < weekDay; i++) {
        out << std::setw(WIDTH) << "";
    }

    unsigned indexWeek = weekDay % 7;
    for (auto i = 0U; i < sizeOfMonth; i++) {
        out << std::setw(WIDTH) << monthDetail[i].day;
        if (++indexWeek % 7 == 0) {
            out << std::endl;
            indexWeek = 0;
        }


    }

}

unsigned Calenda::getWeekDay(unsigned y, unsigned m, unsigned d) const {

    if (m == 1 || m == 2) {
        m += 12;
        y--;
    }
    unsigned iWeek = (d + 2 * m + 3 * (m + 1) / 5 + y + y / 4 - y / 100 + y / 400) % 7;
    return iWeek + 1;
}
