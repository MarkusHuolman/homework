#pragma once

#include <string>
#include <sstream>
#include <iomanip>
#include "Language.hxx"
#include "Date.hxx"

using namespace std;
using namespace std::string_literals;


class Calendar {
    const unsigned year;
    const unsigned month;
    const string   EOL = "\n"s;

    void renderYearMonthHeader(ostringstream& buf, const Language& lang) const {
        Date d{year, month, 1};
        buf << d.year() << " " << lang.month(d.month());
    }

    void renderWeekHeader(ostringstream& buf, const Language& lang) const {
        for (auto k = 1U; k <= 7; ++k)
            buf << (k > 1 ? " " : "") << lang.weekday(k);
    }

    void renderCalendar(ostringstream& buf, const Language& lang) const {
        const unsigned daysPerWeek = 7;
        const unsigned weekStart   = lang.getWeekdayOffset();
        const unsigned numBlanks   = (offset() - weekStart + daysPerWeek) % daysPerWeek;
        const unsigned numDays     = days();
        const int      width       = 2;
        const int      margin      = 1;
        const char     ZERO        = '0';
        unsigned       weekDay     = 0;

        for (auto k = 0U; k < numBlanks; ++k, ++weekDay) {
            pad(buf, width + margin);
        }

        for (auto day = 1U; day <= numDays; ++day, ++weekDay) {
            if (weekDay > 0 && (weekDay % daysPerWeek) == 0) {
                buf << EOL;
            } else {
                if (day > 1U) {
                    pad(buf, margin);
                }
            }
            buf << setw(width) << setfill(ZERO) << day;
        }

        for (; (weekDay % daysPerWeek) != 0; ++weekDay) {
            pad(buf, width + margin);
        }
    }

    void pad(ostringstream& buf, unsigned w) const {
        static constexpr auto SPACE = ' ';
        static constexpr auto EMPTY = "";
        buf << setw(w) << setfill(SPACE) << EMPTY;
    }


public:
    Calendar(unsigned year, unsigned month) : year{year}, month{month} {}

    unsigned days() const {
        return Date{year, month, 1}.last().day();
    }

    unsigned offset() const {
        return Date{year, month, 1}.weekday();
    }

    string render(const Language& lang) const {
        ostringstream buf;
        renderYearMonthHeader(buf, lang);
        buf << EOL;
        renderWeekHeader(buf, lang);
        buf << EOL;
        renderCalendar(buf, lang);
        return buf.str();
    }


    // --- unit test names ---
    friend class render_may_Test;
    friend class render_april_Test;
    friend class render_monthHdr_Test;
    friend class render_weekHdr_Test;
};

