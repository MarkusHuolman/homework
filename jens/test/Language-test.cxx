#include <string>
#include <sstream>
#include "gtest/gtest.h"
#include "../Language.hxx"

using namespace std;
using namespace std::string_literals;
using testing::Test;


Language english{
        {"Sun"s,     "Mon"s,      "Tue"s,   "Wed"s,   "Thu"s, "Fri"s,  "Sat"s},
        {"January"s, "February"s, "March"s, "April"s, "May"s, "June"s, "July"s,
                "August"s, "September"s, "October"s, "November"s, "December"s}
};


TEST(eng, weekStartsWithMonday) {
    string expected = "Mon Tue Wed Thu Fri Sat Sun"s;

    english.setWeekdayOffset(1);
    ostringstream buf;
    for (auto     k = 1U; k <= 7; ++k)
        buf << (k > 1 ? " " : "") << english.weekday(k);

    EXPECT_EQ(buf.str(), expected);
}

TEST(eng, weekStartsWithSunday) {
    string expected = "Sun Mon Tue Wed Thu Fri Sat"s;

    english.setWeekdayOffset(0);
    ostringstream buf;
    for (auto     k = 1U; k <= 7; ++k)
        buf << (k > 1 ? " " : "") << english.weekday(k);

    EXPECT_EQ(buf.str(), expected);
}

TEST(eng, firstAndLastMonths) {
    EXPECT_EQ(english.month(1), "January"s);
    EXPECT_EQ(english.month(12), "December"s);
}




