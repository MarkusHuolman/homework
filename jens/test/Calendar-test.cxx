#include <iostream>
#include <sstream>
#include <string>
#include "gtest/gtest.h"
#include "../Calendar.hxx"

using namespace std;
using namespace std::string_literals;
using testing::Test;

TEST(meta, numDays) {
    EXPECT_EQ((Calendar{2018, 5}.days()), 31U);
    EXPECT_EQ((Calendar{2018, 4}.days()), 30U);
    EXPECT_EQ((Calendar{2018, 2}.days()), 28U);
    EXPECT_EQ((Calendar{2016, 2}.days()), 29U);
}

TEST(meta, offset) {
    EXPECT_EQ((Calendar{2018, 5}.offset()), 2U); //Tuesday
    EXPECT_EQ((Calendar{2018, 4}.offset()), 0U); //Sunday
    EXPECT_EQ((Calendar{2018, 2}.offset()), 4U); //Thursday
    EXPECT_EQ((Calendar{2016, 2}.offset()), 1U); //Monday
}

Language ENG{
        {"Su"s,     "Mo"s,      "Tu"s,   "We"s,   "Th"s, "Fr"s,  "Sa"s},
        {"January"s, "February"s, "March"s, "April"s, "May"s, "June"s, "July"s,
                "August"s, "September"s, "October"s, "November"s, "December"s}
};

TEST(render, may) {
    Calendar may{2018, 5};

    string expected = R"(
   01 02 03 04 05 06
07 08 09 10 11 12 13
14 15 16 17 18 19 20
21 22 23 24 25 26 27
28 29 30 31         )";

    ostringstream buf;
    may.renderCalendar(buf, ENG);
    EXPECT_EQ(buf.str(), expected.substr(1));
}


TEST(render, april) {
    Calendar apr{2018, 4};

    string expected = R"(
                  01
02 03 04 05 06 07 08
09 10 11 12 13 14 15
16 17 18 19 20 21 22
23 24 25 26 27 28 29
30                  )";

    ostringstream buf;
    apr.renderCalendar(buf, ENG);
    EXPECT_EQ(buf.str(), expected.substr(1));
}




TEST(render, monthHdr) {
    Calendar apr{2018, 4};
    string expected = "2018 April"s;

    ostringstream buf;
    apr.renderYearMonthHeader(buf, ENG);
    EXPECT_EQ(buf.str(), expected);
}

TEST(render, weekHdr) {
    Calendar apr{2018, 4};
    string expected = "Mo Tu We Th Fr Sa Su"s;

    ostringstream buf;
    apr.renderWeekHeader(buf, ENG);
    EXPECT_EQ(buf.str(), expected);
}


TEST(render, calendar) {
    Calendar apr{2018, 4};

    string expected = R"(
2018 April
Mo Tu We Th Fr Sa Su
                  01
02 03 04 05 06 07 08
09 10 11 12 13 14 15
16 17 18 19 20 21 22
23 24 25 26 27 28 29
30                  )";

    EXPECT_EQ(apr.render(ENG), expected.substr(1));
}



