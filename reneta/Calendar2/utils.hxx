//
// Created by ebcrens on 2018-05-04.
//
#pragma once
#include <string>
#include <algorithm>
#include <chrono>
using namespace std;

const string regexMonthInterval = "^(([1-9]|[1][0-1])[-]([2-9]|[1][0-2]))$";
const string delimiter = "-";

tm *getCurrentTime () {
    auto now = chrono::system_clock::now();
    time_t now_c = chrono::system_clock::to_time_t(now);
    struct tm *parts = localtime(&now_c);
    return parts;
}

unsigned short getCurrentYear () {
    struct tm *current = getCurrentTime();
    auto currentYear = static_cast<unsigned short>(1900 + current->tm_year);
    return currentYear;
}

unsigned short getCurrentMonth () {
    struct tm *current = getCurrentTime();
    auto currentMonth = static_cast<unsigned short>(1 + current->tm_mon);
    return currentMonth;
}

bool is_number (const string &s) {
    return !s.empty() && find_if(s.begin(), s.end(), [] (char c) {
        return !isdigit(c);
    }) == s.end();
}

bool is_alpha (const string &s) {
    return !s.empty() && find_if(s.begin(), s.end(), [] (char c) {
        return !isalpha(c);
    }) == s.end();
}

bool isValidInterval (const string &interval) {
    regex r(regexMonthInterval);
    return regex_match(interval, r);
}
void validateInterval (const string &intervalString) {
    if (is_number(intervalString)) {
        throw invalid_argument("Interval should be given in format: start-stop not"s + intervalString);
    }
    if (!isValidInterval(intervalString)) {
        throw invalid_argument("Interval should be in format start-stop and between 1-12 not "s + intervalString);
    }
}

pair<unsigned short, unsigned short> convertIntervalToInts (const string &interval) {
    auto start = 0U;
    auto end = interval.find(delimiter);
    string intFirstS;
    while (end != string::npos) {
        intFirstS = interval.substr(start, end - start);
        start = end + delimiter.length();
        end = interval.find(delimiter, start);
    }
    string intSecondS = interval.substr(start, end);
    pair<unsigned short, unsigned short> p;
    p.first = static_cast<unsigned short>(stoi(intFirstS));
    p.second = static_cast<unsigned short>(stoi(intSecondS));
    if (p.first > p.second) {
        throw invalid_argument("Interval start "s +  to_string(p.first) + " should be less than stop "s + to_string(p.second) );
    }
    return p;
}
