#include <ctime>
#include <iomanip>
#include <iostream>
#include <string>
#include <vector>
#include "helpers.hxx"
#include "Language.hxx"


using namespace std;
using namespace literals;

int main(int argc, char *argv[]) {


    // Create all languages
    // ====================
    vector<Language> languages = createLanguages();


    // Default values
    // ==============
    unsigned fromYear;
    unsigned fromMonthNo; // January=0
    {
        time_t now = time(nullptr);
        tm *localTime = localtime(&now);
        fromYear = static_cast<unsigned>(1900 + localTime->tm_year);
        fromMonthNo = static_cast<unsigned>(localTime->tm_mon);
    }
    unsigned toYear = 0;
    unsigned toMonthNo = 0;
    Language language = getLanguage(languages, "eng");
    unsigned startDay = 1; // Sunday=0, Monday=1 ...


    // Read and store command arguments
    // ================================
    string givenFromMonth;
    string givenToMonth;
    string givenStartDay;
    if (argc > 1) {
        for (int i = 0; i < argc; ++i) {
            string arg = argv[i];
            if (arg == "-y") { fromYear = static_cast<unsigned int>(stoi(argv[i + 1])); }
            if (arg == "-m") { givenFromMonth = argv[i + 1]; }
            if (arg == "-yT") { toYear = static_cast<unsigned int>(stoi(argv[i + 1])); }
            if (arg == "-mT") { givenToMonth = argv[i + 1]; }
            if (arg == "-l") { language = getLanguage(languages, argv[i + 1]); }
            if (arg == "-sd") { givenStartDay = argv[i + 1]; }
            if (arg == "-h") { printHelp(); return 0;}
        }
    }


    // TODO: Remove later
//    fromYear = 2018;
//    fromMonthNo = 0;
//    toYear = 2018;
//    toMonthNo = 5;
//    language = getLanguage(languages, "esp");
//    startDay = 0;


    // Values to set after knowing the input
    // =====================================
    if (!givenFromMonth.empty()) { fromMonthNo = language.getMonthNo(givenFromMonth); }
    if (!givenToMonth.empty()) { toMonthNo = language.getMonthNo(givenToMonth); }
    if (!givenStartDay.empty()) { startDay = language.getWeekDayNo(givenStartDay); }
    if (toYear == 0) { toYear = fromYear; }
    if (toMonthNo == 0) { toMonthNo = fromMonthNo; }
    vector<pair<unsigned, unsigned>> allYearMonth = createAllYearsMonth(fromYear, toYear, fromMonthNo, toMonthNo);


    // Print all information
    // =====================
    for (auto yearMonth : allYearMonth) {
        unsigned year = yearMonth.first;
        unsigned monthNo = yearMonth.second;

        // Month heading
        string heading = language.getMonth(monthNo) + " " + to_string(year) + " ";
        cout << left << setfill('=') << setw(27) << heading << endl;

        // Weekdays heading
        cout << genWeekDays(language, startDay) << endl;

        // All days
        monthInfo thisMontInfo = getMonthInfo(year, monthNo);
        printMonth(thisMontInfo, startDay);
        cout << endl;
    }

    return 0;
}