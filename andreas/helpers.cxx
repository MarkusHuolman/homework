#include <iomanip>
#include <iostream>
#include "helpers.hxx"

vector<pair<unsigned, unsigned>> createAllYearsMonth(unsigned fromYear, unsigned toYear, unsigned fromMonthNo, unsigned toMonthNo) {

    vector<pair<unsigned, unsigned>> result;

    for (auto year = fromYear; year <= toYear; ++year) {
        unsigned monthNo1;
        unsigned monthNo2;

        if (fromYear == toYear) {
            monthNo1 = fromMonthNo;
            monthNo2 = toMonthNo;
        } else if (year == fromYear && fromYear != toYear) {
            monthNo1 = fromMonthNo;
            monthNo2 = 11;
        } else if (year == toYear && fromYear != toYear) {
            monthNo1 = 0;
            monthNo2 = toMonthNo;
        } else {
            monthNo1 = 0;
            monthNo2 = 0;
        }

        for (auto monthNo = monthNo1; monthNo <= monthNo2; ++monthNo) {
            pair<unsigned, unsigned> oneYearMonth = {year, monthNo};
            result.push_back(oneYearMonth);
        }
    }

    return result;
};


string genWeekDays(const Language &language, const unsigned startDay) {
    string result;

    for (int wd = startDay; wd <= 6 ; ++wd) {
        if (wd == 0 ) { result += "\033[0;31m"; }
        result += language.getWeekDay(wd) + " ";
        if (wd == 0 ) { result += "\033[0m"; }
    }

    if (startDay != 0) {
        for (auto wd = 0U; wd <= startDay - 1; ++wd) {
            if (wd == 0 ) { result += "\033[0;31m"; }
            result += language.getWeekDay(wd) + " ";
            if (wd == 0 ) { result += "\033[0m"; }
        }
    }

    return result;
}


monthInfo getMonthInfo(unsigned year, unsigned monthNo, unsigned dayNo) {

    monthInfo result {};

    // Setup time variable for the given year and month (day 1)
    tm thatTime = { 0, 0, 0, static_cast<int>(dayNo),static_cast<int>( monthNo), static_cast<int>(year) - 1900 }; // second, minute, hour, 1-based day, 0-based month, year since 1900
    time_t tmpTime = mktime(&thatTime);
    const tm * usedTime = localtime(&tmpTime);

    result.firstWeekDay = static_cast<unsigned>(usedTime->tm_wday);

    // Find last day of month
    auto thisMonth = static_cast<unsigned>(usedTime->tm_mon);
    unsigned lastDay;
    do {
        lastDay = static_cast<unsigned>(usedTime->tm_mday);

        tmpTime += (24 * 60 *60);
        usedTime = localtime(&tmpTime);
    } while (static_cast<unsigned>(usedTime->tm_mon) == thisMonth);
    result.lastDay = lastDay;

    return result;

}


void printHelp() {
    cout << "Arguments to the command:" << endl;
    cout << "  -h:  Shows this help." << endl;
    cout << "  -l:  language. Eg. eng (default), swe, esp , ger ..." << endl;
    cout << "  -m:  month (from). Name of month in given language. Eg. June, Maj, Abril, März ..." << endl;
    cout << "  -mT: month to. Same as above. Will print month between -m and -mT." << endl;
    cout << "  -sd: start day of week in given language. Eg. Sun, Mån, Mar, Don ..." << endl;
    cout << "  -y:  year (from)." << endl;
    cout << "  -yT: year to. Will print years between -y and -yT." << endl;
    cout << endl;
    cout << "No checking of arguments are implemented!!!" << endl << endl;
    cout << "Ready!" << endl;
}


void printMonth(monthInfo monthInfo, unsigned startDay) {

    auto printPos = 0U;

    // Print empty days before this month start
    unsigned stopPos = monthInfo.firstWeekDay >= startDay ? monthInfo.firstWeekDay - startDay : 7 - (startDay - monthInfo.firstWeekDay);
    for (; printPos < stopPos; ++printPos){
        cout << right << setfill(' ') << setw(3) << " " << " ";
    }

    // Print the month
    for (auto day = 1U; day <= monthInfo.lastDay; ++day) {
        cout << right << setfill(' ') << setw(3) << day << " ";
        if (++printPos >= 7) {
            cout << endl;
            printPos = 0;
        }
    }
    cout << endl;
}