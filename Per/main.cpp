#include <iostream>
#include <iomanip>
#include <ctime>
#include <string>
#include <algorithm>
#include <cstring>
#include <string>
#include <iostream>
#include <algorithm>
#include <cctype>
#include <cwctype>
#include <sstream>

using namespace std;

const char *weekday[][7] = {{"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"},
                            {"Sun", "Mån", "Tis", "Ons", "Tor", "Fre", "Lör"}};

const char *monthName[][12] = {{"January", "February", "March", "April", "May", "June", "July", "August",  "September", "October", "November", "December"},
                               {"Januari", "Februari", "Mars",  "April", "Maj", "Juni", "Juli", "Augusti", "September", "Oktober", "November", "December"}};

inline bool caseInsCharCompareN(char a, char b) {
    return (toupper(a) == toupper(b));
}

bool caseInsCompare(const string &s1, const string &s2) {
    return ((s1.size() == s2.size()) &&
            equal(s1.begin(), s1.end(), s2.begin(), caseInsCharCompareN));
}


int isItInteger(char *stuff) {
    char *endp;
    long i = strtol(stuff, &endp, 10);

    if (!*endp) {
        std::istringstream iss(stuff);
        int val;
        if (iss >> val) {
            return val;
        }
    }
    return -1;
}

int main(int argc, char *argv[]) {

    int lang = 0;
    struct tm *timeinfo;
    time_t t = std::time(0);    // get time now
    timeinfo = std::localtime(&t);
    timeinfo->tm_mday = 1;      //set date to 1st day of current month
    mktime(timeinfo);
    int numberOfMonths = 1;

    for (int ii = 1; ii < argc; ii++) {
        if (!strcmp(argv[ii],"-h")) {
            cout << "Usage: "<< argv[0] << " [-h help] [-y year] [Month name] [-c # of months] [-swe lang swedish]\n";
            return 0;
        }
            if (!strcmp(argv[ii],"-swe")) { lang = 1;
            cout << "**a="<<argv[ii] <<"\n";
        }
        cout << "a="<<argv[ii] <<"\n";
    }

    for (int ii = 1; ii < argc; ii++) {

        for (int i = 0; i < 12; i++) {
            if (caseInsCompare(argv[ii], monthName[lang][i])) {
                timeinfo->tm_mon = i;
            }
        }
        if(argv[ii+1]!=NULL) {
            if (!strcmp(argv[ii], "-y")) {
                int val = isItInteger(argv[ii + 1]);
                if (val != -1) {
                    timeinfo->tm_year = val - 1900;
                }
            }

            if (!strcmp(argv[ii], "-c")) {
                int val = isItInteger(argv[ii + 1]);
                if (val != -1) {
                    numberOfMonths = val;
                }
            }
        }
    }

    mktime(timeinfo);
    int year  = timeinfo->tm_year;
    //    int year  = timeinfo->tm_year + 1900; //Wierd result...
    int month = timeinfo->tm_mon;

    for (int o = 1; o <= numberOfMonths; o++) {
        if (month > 11) {
            year++;
            month = 0;
        }

        timeinfo->tm_year = year;
        timeinfo->tm_mon = month;

        cout << "\n\n"<< monthName[lang][month] << " " << (year+1900) << "\n";

        for (int i = 0; i < 6; i++) {
            cout << weekday[lang][i] << " ";    // output Weekday + space character
        }
        cout << weekday[lang][6] << "\n";

        int curMonth = timeinfo->tm_mon;
        int pos = 1;

        for (int i = 1; i < 32; i++, pos++) {

            timeinfo->tm_mday = i;
            mktime(timeinfo);
            if (timeinfo->tm_mon > curMonth) break;  //break loop if end of month

            if (i == 1) {           //print space chars to align 1st of Month to correct weekday
                for (int i = 0; i < timeinfo->tm_wday; i++) {
                    cout << "    ";  pos++;
                }
            }

            if (pos > 7) {          //print newline after each 7 days
                pos = 1;
                cout << "\n";
            }
            printf("%3d ", i);     //print day, 3 chars and right adjusted
        }
        month++;
    }

    cout << "\n";
    return 0;
}